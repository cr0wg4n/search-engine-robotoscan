from flask import Flask, jsonify, request
from multiprocessing import Value
import requests
import json
from urllib.parse import urlencode
from flask_cors import CORS
import math 
from datetime import datetime

counter = Value('i', 0)
app = Flask(__name__)
CORS(app)

void_words = ["a","acuerdo","adelante","ademas","además","adrede",
    "ahi","ahí","ahora","al","alli","allí","alrededor","antano","antaño",
    "ante","antes","apenas","aproximadamente","aquel","aquél","aquella",
    "aquélla","aquellas","aquéllas","aquello","aquellos","aquéllos","aqui",
    "aquí","arriba","abajo","asi","así","aun","aún","aunque","b","bajo",
    "bastante","bien","breve","c","casi","cerca","claro","como","cómo","con",
    "conmigo","contigo","contra","cual","cuál","cuales","cuáles","cuando",
    "cuándo","cuanta","cuánta","cuantas","cuántas","cuanto","cuánto","cuantos",
    "cuántos","d","de","debajo","del","delante","demasiado","dentro","deprisa",
    "desde","despacio","despues","después","detras","detrás","dia","día","dias",
    "días","donde","dónde","dos","durante","e","el","él","ella","ellas","ellos",
    "en","encima","enfrente","enseguida","entre","es","esa","ésa","esas","ésas",
    "ese","ése","eso","esos","ésos","esta","está","ésta","estado","estados",
    "estan","están","estar","estas","éstas","este","éste","esto","estos","éstos",
    "ex","excepto","f","final","fue","fuera","fueron","g","general","gran","h",
    "ha","habia","había","habla","hablan","hace","hacia","han","hasta","hay",
    "horas","hoy","i","incluso","informo","informó","j","junto","k","l","la",
    "lado","las","le","lejos","lo","los","luego","m","mal","mas","más","mayor",
    "me","medio","mejor","menos","menudo","mi","mí","mia","mía","mias","mías",
    "mientras","mio","mío","mios","míos","mis","mismo","mucho","muy","n","nada",
    "nadie","ninguna","no","nos","nosotras","nosotros","nuestra","nuestras","nuestro",
    "nuestros","nueva","nuevo","nunca","o","os","otra","otros","p","pais","paìs",
    "para","parte","pasado","peor","pero","poco","por","porque","pronto","proximo",
    "próximo","puede","q","qeu","que","qué","quien","quién","quienes","quiénes",
    "quiza","quizá","quizas","quizás","r","raras","repente","s","salvo","se","sé",
    "segun","según","ser","sera","será","si","sí","sido","siempre","sin","sobre",
    "solamente","solo","sólo","son","soyos","su","supuesto","sus","suya","suyas",
    "suyo","t","tal","tambien","también","tampoco","tarde","te","temprano","ti",
    "tiene","todavia","todavía","todo","todos","tras","tu","tú","tus","tuya",
    "tuyas","tuyo","tuyos","u","un","una","unas","uno","unos","usted","ustedes",
    "v","veces","vez","vosotras","vosotros","vuestra","vuestras","vuestro",
    "vuestros","w","x","y","ya","yo","z"]

url_to_tokenizer= 'http://localhost:3500/api/IndexUrls'
urls='http://localhost:3500/api/Urls/'
url_tokenized = 'http://localhost:3500/api/TokenizedContents'
result_url = 'http://localhost:3500/api/Urls/'

class Word:
    docAparition=[]
    body = ''
    isQuery = False
    weight = 0
    def getAparitions(self):
        count = 0
        for aparition in self.docAparition:
            if aparition > 0:
                count = count + 1
        return count
    def defineWeight(self):
        if self.isQuery==True:
            aparition = self.getAparitions()-1
        else:
            aparition = self.getAparitions()
        if aparition > 0:
            w = math.log10(len(self.docAparition)-1/aparition)
        else:
            w = 0
        self.weight = w
    def autoWeight(self):
        weights = []
        self.defineWeight()
        for aparition in self.docAparition:
            weights.append(aparition*self.weight)
        self.docAparition = weights

def formatResult(results):
    response = []
    for result in results:
        stringUrl = urlencode({'':result['id']})
        stringUrl = stringUrl[1:]
        res = requests.get(result_url+stringUrl)
        data = res.json()
        trim = {'data':data, 'simility':result['simility']}
        response.append(trim)
    now = datetime.now()
    time = now.strftime("%H:%M:%S")
    endData = {'response':response,'timestamp':time }
    return endData

def vectorialModel(ids,aparitionsWeight):
    res = [] 
    lon = len(ids)-1
    for i in range(lon):
        aux = 0
        for j in range(len(aparitionsWeight)):
            aux += aparitionsWeight[j][i]*aparitionsWeight[j][lon]
        data = {'id':ids[i], 'simility':aux}
        res.append(data)
    return res

def counterWord(reducts,query):
    big = []
    for reduct in reducts:
        for content in reduct['content']:
            big.append(content)
    big = list(dict.fromkeys(big))
    listOfWords = []
    extraQuery = {'id': 'query', 'content': query} 
    reducts.append(extraQuery)
    for item in big:
        word = Word()
        word.body = item
        aparitions=[]
        listID = []
        for reduct in reducts:
            count = 0
            for content in reduct['content']:
                if content == item:
                    count = count +1
                if content in query:
                    word.isQuery = True
            aparitions.append(count)
            listID.append(reduct['id'])
        word.docAparition = aparitions
        listOfWords.append(word)
        dataResult = {'ids':listID,'aparitionsWord':listOfWords} ##listID !=listofWords
    return dataResult

def binaryReduct(querys):
    response = requests.get(url_tokenized)
    documents =response.json()
    reduct = []
    for query in querys:
        queryList = []
        for doc in documents:
            words = doc['content']
            words = words.split()
            cont  = 0
            for word in words:
                if query == word:
                    cont = cont + 1
                    break
            data = { 'id': doc['urlID'],'content': words, 'exist': cont }
            queryList.append(data)
        reduct.append(queryList)
    filterList = []
    if len(reduct)!= 0:
        for i in range(len(reduct[0])):
            count = 0
            for j in range(len(reduct)):
                if reduct[j][i]['exist'] > 0:
                    count = count + 1
            if count >=1:
                data = { 'id': reduct[0][i]['id'], 'content': reduct[0][i]['content'] }
                filterList.append(data)
    return filterList 

def token(string):
    start = 0
    i = 0
    token_list = []
    for x in range(0, len(string)):
        if " " == string[i:i+1][0]:
            token_list.append(string[start:i+1])
            start = i + 1
        i += 1
    token_list.append(string[start:i+1])
    token_list_end = []
    for i in range(len(token_list)):
        if token_list[i] != '' and token_list[i] != ' ' and token_list[i] != '  ' and token_list[i] != '*' and token_list[i] != '/':
            flag = False
            token_list[i] = token_list[i].strip()
            for word in void_words:
                if word == token_list[i] or token_list[i][:1]=='[' or token_list[i][:4]=='http':
                    flag=True
                    break
            if flag == False:
                token_list_end.append(token_list[i])  

    res = ''
    for string in token_list_end:
        res= res+' '+string
    return res

def tokenQuery(string):
    start = 0
    i = 0
    token_list = []
    for x in range(0, len(string)):
        if " " == string[i:i+1][0]:
            pal = string[start:i+1].strip()
            if pal != '':
                token_list.append(pal)
            start = i + 1
        i += 1
    pal = string[start:i+1].strip()
    if pal != '':
        token_list.append(pal)
    return token_list

@app.route('/api/tokenizer', methods=['GET'])
def tokenize():
    r = requests.get(url_to_tokenizer)
    data =r.json()
    for item in data:
        if item['indexed'] == False:
            url = item['url']
            stringUrl = urlencode({'':url})
            stringUrl = stringUrl[1:]
            req = requests.get(urls+stringUrl+'/Pcontent')
            try:
                content = req.json()[0]
                content_string = content['content']
                content_string = content_string.replace('\n',' ')    
                words = token(content_string)
                data = {
                    'urlID': url,
                    'content': words
                }
                req = requests.post(url_tokenized,data)
            except:
                print (stringUrl)
    return jsonify('done')

@app.route('/api/query', methods=['GET'])
def querys():
    r=requests.get(url_to_tokenizer)
    data = r.json()
    lista= []
    for item in data:
        lista.append(item['url'])
    return jsonify(lista)

@app.route('/api/querySearch', methods=['POST'])
def searchQuery():
    payload = request.json
    query = payload['query']
    reduct = binaryReduct(query)
    if reduct != []:
        reduct = counterWord(reduct,query) #word class
        ids = reduct['ids']
        cont = reduct['aparitionsWord']
        p = []
        for item in cont:
            item.autoWeight()
            p.append(item.docAparition)
        res = vectorialModel(ids,p)
        return jsonify(formatResult(res))
    else:
        return jsonify(reduct)


if __name__ == '__main__':
    app.run()



