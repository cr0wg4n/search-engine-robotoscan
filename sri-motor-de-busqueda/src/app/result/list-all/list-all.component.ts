import { Component, OnInit } from '@angular/core';
import { QueryService } from 'src/app/services/query.service';
import { UrlsService } from 'src/app/services/urls.service';
import { ResultModel } from 'src/app/models/result.model';

@Component({
  selector: 'app-list-all',
  templateUrl: './list-all.component.html',
  styleUrls: ['./list-all.component.css']
})
export class ListAllComponent implements OnInit {
  results: ResultModel[] = [];
  resultNumbers = 0;
  constructor(private queryService: QueryService,private urlsService: UrlsService ) { }

  ngOnInit() {
    this.urlsService.getUrls().subscribe(
      (data) => {
        data.forEach(item => {
          const prefab: ResultModel = {
            'id': item.url,
            'title': item.title,
            'url': item.url,
            'description': item.description,
            'simility': 0
          }
          this.results.push(prefab);
          this.resultNumbers = data.length;
        });
      }
    );
  }

}
