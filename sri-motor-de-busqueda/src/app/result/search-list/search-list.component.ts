import { Component, OnInit } from '@angular/core';
import { ResultModel } from 'src/app/models/result.model';
import { ResultsService } from 'src/app/services/results.service';
import { UrlsService } from 'src/app/services/urls.service';
import { ActivatedRoute, Router } from '@angular/router';
import { QueryService } from 'src/app/services/query.service';
import { QueryModel } from 'src/app/models/query.model';
import { formatDate } from '@angular/common';
import { ResponseModel } from 'src/app/models/response.model';
@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit {
  responseServer: ResponseModel;
  images = false;
  ress: ResultModel[] = [];
  timestamp: string;
  time;
  initialTimestamp: string;
  resultNumbers = 0;
  query = '';
  voidWords = ["a", "acuerdo", "adelante", "ademas", "además", "adrede",
    "ahi", "ahí", "ahora", "al", "alli", "allí", "alrededor", "antano", "antaño",
    "ante", "antes", "apenas", "aproximadamente", "aquel", "aquél", "aquella",
    "aquélla", "aquellas", "aquéllas", "aquello", "aquellos", "aquéllos", "aqui",
    "aquí", "arriba", "abajo", "asi", "así", "aun", "aún", "aunque", "b", "bajo",
    "bastante", "bien", "breve", "c", "casi", "cerca", "claro", "como", "cómo", "con",
    "conmigo", "contigo", "contra", "cual", "cuál", "cuales", "cuáles", "cuando",
    "cuándo", "cuanta", "cuánta", "cuantas", "cuántas", "cuanto", "cuánto", "cuantos",
    "cuántos", "d", "de", "debajo", "del", "delante", "demasiado", "dentro", "deprisa",
    "desde", "despacio", "despues", "después", "detras", "detrás", "dia", "día", "dias",
    "días", "donde", "dónde", "dos", "durante", "e", "el", "él", "ella", "ellas", "ellos",
    "en", "encima", "enfrente", "enseguida", "entre", "es", "esa", "ésa", "esas", "ésas",
    "ese", "ése", "eso", "esos", "ésos", "esta", "está", "ésta", "estado", "estados",
    "estan", "están", "estar", "estas", "éstas", "este", "éste", "esto", "estos", "éstos",
    "ex", "excepto", "f", "final", "fue", "fuera", "fueron", "g", "general", "gran", "h",
    "ha", "habia", "había", "habla", "hablan", "hace", "hacia", "han", "hasta", "hay",
    "horas", "hoy", "i", "incluso", "informo", "informó", "j", "junto", "k", "l", "la",
    "lado", "las", "le", "lejos", "lo", "los", "luego", "m", "mal", "mas", "más", "mayor",
    "me", "medio", "mejor", "menos", "menudo", "mi", "mí", "mia", "mía", "mias", "mías",
    "mientras", "mio", "mío", "mios", "míos", "mis", "mismo", "mucho", "muy", "n", "nada",
    "nadie", "ninguna", "no", "nos", "nosotras", "nosotros", "nuestra", "nuestras", "nuestro",
    "nuestros", "nueva", "nuevo", "nunca", "o", "os", "otra", "otros", "p", "pais", "paìs",
    "para", "parte", "pasado", "peor", "pero", "poco", "por", "porque", "pronto", "proximo",
    "próximo", "puede", "q", "qeu", "que", "qué", "quien", "quién", "quienes", "quiénes",
    "quiza", "quizá", "quizas", "quizás", "r", "raras", "repente", "s", "salvo", "se", "sé",
    "segun", "según", "ser", "sera", "será", "si", "sí", "sido", "siempre", "sin", "sobre",
    "solamente", "solo", "sólo", "son", "soyos", "su", "supuesto", "sus", "suya", "suyas",
    "suyo", "t", "tal", "tambien", "también", "tampoco", "tarde", "te", "temprano", "ti",
    "tiene", "todavia", "todavía", "todo", "todos", "tras", "tu", "tú", "tus", "tuya",
    "tuyas", "tuyo", "tuyos", "u", "un", "una", "unas", "uno", "unos", "usted", "ustedes",
    "v", "veces", "vez", "vosotras", "vosotros", "vuestra", "vuestras", "vuestro",
    "vuestros", "w", "x", "y", "ya", "yo", "z"];


  constructor(private resultService: ResultsService,
    private urlsService: UrlsService,
    private route: ActivatedRoute,
    private router: Router,
    private queryService: QueryService
  ) { }
  ngOnInit() {
    this.route.params.subscribe(
      (data) => {
        this.query = data.id;
        const q: QueryModel = {
          query: this.clearQuery(this.query)
        };
        this.initialTimestamp = formatDate(Date.now(), 'H:mm:ss', 'en-US');
        this.queryService.sendQuery(q).subscribe(
          (response) => {
            console.log(response)
            this.responseServer = response;
            this.ress = this.listResult(response.response);
            console.log( this.ress );
            this.timestamp = response.timestamp;
            this.time = this.diferenceTime(this.initialTimestamp, this.timestamp);
            this.orderByRank(this.ress);
            this.resultNumbers = this.ress.length;
          }
        );
      }
    );
  }
  listResult(object){
    const list: ResultModel[] = [];
    object.forEach(result => {
      const obj:ResultModel = result.data;
      obj.simility = result.simility;
      list.push(obj);
    });
    return list;
  }
  orderByRank(list) {
    for (let i = 0; i < list.length; i++) {
        for (let j = 0; j < list.length - 1 - i; j++) {
            if ( list[j].simility < list[j + 1].simility) {
                const aux = list[j + 1].simility;
                list[j + 1].simility = list[j].simility;
                list[j].simility = aux;
            }
        }
    }
    return list;
}
  search() {
    this.router.navigate(['/results', this.query]);
  }
  clearQuery(text) {
    const res = [];
    text = text.split(' ');
    text.forEach(word => {
      if ( word === '' ) {
        return;
      }
      let flag = false;
      for (let i = 0; i < this.voidWords.length; i++) {
        if (word === this.voidWords[i]) {
          flag = true;
          break;
        }
      }
      if (flag === false) {
        res.push(word);
      }
    });
    return res;
  }
  diferenceTime(start,end){
    const h1 = start.split(':');
    const h2 = end.split(':');
    const list = []
    for (let i = 0; i < h1.length; i++) {
      const n = parseInt(h2[i]) - parseInt(h1[i]);
      list.push(n);
    }
    let res = '';
    for (let i = 0; i < list.length; i++) {
      if (list[i] < 10) {
        res = res + ':0' + list[i];
      } else {
        res = res + ':' + list[i];
      }
    }
    return res.substring(1, res.length);
  }
  toggle(){
    this.images = !this.images;
  }
  all() {
    this.router.navigate(['/all']);
  }
}
