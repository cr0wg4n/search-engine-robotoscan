import { Component, OnInit, Input } from '@angular/core';
import { ResultModel } from 'src/app/models/result.model';

@Component({
  selector: 'app-search-card',
  templateUrl: './search-card.component.html',
  styleUrls: ['./search-card.component.css']
})
export class SearchCardComponent implements OnInit {

  @Input()
  result:ResultModel;
  constructor() { }
  ngOnInit() {
  }

}
