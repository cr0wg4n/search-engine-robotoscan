import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UrlsModel } from '../models/urls.model';

@Injectable({
  providedIn: 'root'
})
export class UrlsService {
  url: string = environment.urls;
  constructor(private http: HttpClient) { }
  getUrls(): Observable<UrlsModel[]> {
    return this.http.get<UrlsModel[]>(this.url);
  }
  getUrl(id: number): Observable<UrlsModel> {
    const url = `${this.url}/${id}`;
    return this.http.get<UrlsModel>(url);
  }
  addUrl(document): Observable<UrlsModel> {
    return this.http.post<UrlsModel>(this.url, document);
  }
  deleteUrl(id: number): Observable<UrlsModel> {
    return this.http.delete<UrlsModel>(`${this.url}/${id}`);
  }
}
