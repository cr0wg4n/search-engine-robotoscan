import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResultModel } from '../models/result.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResultsService {
  url: string = environment.urlQuery;
  constructor(private http: HttpClient) {}

  getResults(): Observable<ResultModel[]>{
    return this.http.get<ResultModel[]>(this.url);
  }
  getResult(id: number): Observable<ResultModel>{
    const url = `${this.url}/${id}`;
    return this.http.get<ResultModel>(url);
  }
  addResult(document): Observable<ResultModel>{
    return this.http.post<ResultModel>(this.url, document);
  }
  deleteResult(id: number): Observable<ResultModel>{
    return this.http.delete<ResultModel>(`${this.url}/${id}`);
  }
}
