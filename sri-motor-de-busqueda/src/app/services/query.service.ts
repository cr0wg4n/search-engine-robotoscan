import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QueryModel } from '../models/query.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QueryService {
  url: string = environment.urlQueryServer;
  constructor(private http: HttpClient) {}
  sendQuery(data): Observable<any> {
    return this.http.post<any>(this.url, data);
  }
}
