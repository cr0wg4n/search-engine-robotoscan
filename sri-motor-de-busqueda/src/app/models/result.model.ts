export interface ResultModel {
    id?: string;
    title: string;
    url: string;
    description?: string;
    simility?: number;
    image?:string;
}
