export interface UrlsModel {
    url: string;
    title?: string;
    description?: string;
    image?: string;
    date?: string;
    mapID?: string;
    publisher?: string;
}

