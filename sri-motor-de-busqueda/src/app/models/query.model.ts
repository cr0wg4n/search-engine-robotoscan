export interface QueryModel {
    query: string[];
    timestamp?: string;
}
