import { ResultModel } from './result.model';

export interface DataModel {
    data: ResultModel;
    simility: string;
}