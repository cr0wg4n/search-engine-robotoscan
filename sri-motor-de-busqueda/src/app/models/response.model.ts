import { DataModel } from './data.model';

export interface ResponseModel {
    timestamp?: string;
    response: DataModel[];
}
