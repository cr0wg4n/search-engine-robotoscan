import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SearchListComponent } from './result/search-list/search-list.component';
import { ListAllComponent } from './result/list-all/list-all.component';

const routes: Routes = [
 {
   path: '',
   component: HomeComponent
 },
 {
  path: 'results/:id',
  component: SearchListComponent
 }
 ,{
   path: 'all',
   component: ListAllComponent
 }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
