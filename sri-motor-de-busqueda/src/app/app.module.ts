import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { SearchCardComponent } from './result/search-card/search-card.component';
import { SearchListComponent } from './result/search-list/search-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ResultsService } from './services/results.service';
import { UrlsService } from './services/urls.service';
import { QueryService } from './services/query.service';
import { ListAllComponent } from './result/list-all/list-all.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchCardComponent,
    SearchListComponent,
    ListAllComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    ResultsService,
    UrlsService,
    QueryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
