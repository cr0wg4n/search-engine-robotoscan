var extractor = require('unfluff');
var axios = require('axios');
var Sitemapper = require('sitemapper');
var sitemap = new Sitemapper();
const htmlToText = require('html-to-text');

const baseURL='http://localhost:3500/api';
const dbIndexUrls=baseURL+'/IndexUrls';
const dbUrls = baseURL+'/Urls';
const dbTokenizedContent = baseURL+'/TokenizedContents' 
const dbPureContent = baseURL+'/PureContents';
const dbSiteMap = baseURL+'/IndexSitemaps';

//saveSitemapPagesOnUrls();
indexPages();

async function saveSitemapPagesOnUrls(){
  axios.get(dbSiteMap).then(
    (res)=>{
      var response =res.data;
      response.forEach(item=>{
        getSitemapLinks(item.url).then(
          (data)=>{
            data.forEach(link=>{
              var prefab={
                  "url": link,
                  "indexed": false,
                  "sitemap": item.url
              };
              axios.post(dbIndexUrls,prefab).then(
                  (res)=>{
                    console.log('sitemap done');
                  }
              );
            });
          }
        );
      });
    }
  );
}
async function indexPages(){
  axios.get(dbIndexUrls).then(
    (resp)=>{
      var response = resp.data;
      response.forEach(async item => {
        var link = item.url;
        var id = item.id;
        var sitemap = item.sitemap;
        getSiteData(link).then(
          (res)=>{
            var dataSite=res;
            dataSite.mapID=sitemap;
            var prefab={
              "url":link,
              "indexed":false
            };
            axios.put(dbIndexUrls+'/'+id,prefab).then(
              (res)=>{
                getSiteContent(link).then(
                  (res)=>{
                    var pureContent={
                      "content":res,
                      "urlID":link
                    };
                    axios.post(dbPureContent,pureContent).then(
                      (res)=>{
                        axios.post(dbUrls,dataSite).then(
                          (res)=>{
                            console.log('exitoso');
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          }
        );
      });
    }
  );
}
async function getSiteData(link){
  const response = await axios.get(link);
  //const data = await extractor(response.data);
  const data = await extractor.lazy(response.data);
  var pureDescription = data.description();
  pureDescription = htmlToText.fromString(pureDescription,
    {
      wordwrap: 130
    }
  ); 
  var htmldata={
    "url": link,
    "title": data.title(),
    "description": pureDescription,
    "image": data.image(),
    "date": data.date(),
    "publisher": data.publisher()
  };
  return htmldata;
}
async function getSiteContent(link){
  const response = await axios.get(link);
  const text = htmlToText.fromString(response.data, {
    wordwrap: 130
  });
  return text;
}
async function getSitemapLinks(link){
  const site = await sitemap.fetch(link);
  return site.sites;
}

//getSiteData('https://blog.adrianistan.eu/convertir-a-haiku-en-rolling-release').then((data)=>console.log(data)).catch((e)=>console.log('error'));
//getSiteContent(5).then((data)=>console.log(data)).catch((e)=>console.log('error'));
//getSitemapLinks(1).then((data)=>console.log(data)).catch((e)=>console.log('error'));

